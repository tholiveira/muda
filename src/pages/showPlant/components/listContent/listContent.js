import React from "react";
import { View, Text } from "react-native";
import Style from "../sectionContents/style";
import Line from "../line/line";

const listContent = props => {
  return (
    <View style={Style.eachContentContainer}>
      <Text style={Style.mainTitleContent}>{props.title}</Text>
      <View style={Style.inlineContent}>
        <Text style={Style.mainTitleContent}>{props.titleSubject}</Text>
        <Text style={Style.contentSection}>{props.subject}</Text>
      </View>
      <View style={Style.inlineContent}>
        <Text style={Style.mainTitleContent}>{props.titleSubject2}</Text>
        <Text style={Style.contentSection}>{props.subject2}</Text>
      </View>
      <View style={Style.inlineContent}>
        <Text style={Style.mainTitleContent}>{props.titleSubject3}</Text>
        <Text style={Style.contentSection}>{props.subject3}</Text>
      </View>
      <View style={Style.inlineContent}>
        <Text style={Style.mainTitleContent}>{props.titleSubject4}</Text>
        <Text style={Style.contentSection}>{props.subject4}</Text>
      </View>
      <Line />
    </View>
  );
};

export default listContent;
