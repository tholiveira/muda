import React from "react";
import { View, Text } from "react-native";
import Style from "./style";
import Line from "../line/line";

const sectionContent = props => {
  return (
    <View style={Style.eachContentContainer}>
      <Text style={Style.mainTitleContent}>{props.mainTitleContent}</Text>
      <Text style={Style.contentSection}>{props.subject}</Text>
      <Line />
    </View>
  );
};

export default sectionContent;
