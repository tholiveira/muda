import { StyleSheet } from "react-native";
import Colors from "../../../../themes/colors/colors";

const style = StyleSheet.create({
  eachContentContainer: {
    width: "80%",
    marginBottom: 20
  },
  mainTitleContent: {
    color: Colors.primaryBlack,
    fontSize: 14,
    lineHeight: 16,
    fontFamily: "ubuntu",
    marginRight: 20
  },
  contentSection: {
    color: Colors.primaryGray,
    fontSize: 14,
    lineHeight: 16,
    fontFamily: "ubuntu"
  },
  inlineContent: {
    flexDirection: "row",
    left: 15,
    marginTop: 5
  }
});

export default style;
