import React from "react";
import { StyleSheet } from "react-native";
import Colors from "../../../../themes/colors/colors";

const style = StyleSheet.create({
  line: {
    width: 200,
    height: 1,
    marginTop: 10,
    alignSelf: "center",
    backgroundColor: Colors.primaryGray
  }
});

export default style;
