import React, { useState, useEffect } from "react";
import { Text, View, ScrollView, Image, Button, Alert } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Api from "../../services/plantsApi";
import Style from "./style";
import Content from "./components/sectionContents/sectionContent";
import List from "./components/listContent/listContent";

const showPlant = props => {
  const { navigation } = props;
  const ID = JSON.stringify(navigation.getParam("itemID"));
  const [plant, setPlant] = useState({});
  const [flower, setFlower] = useState({});
  const [foliage, setFoliage] = useState({});
  const [fruitOrSeed, setFruitOrSeed] = useState({});
  const [image, setImage] = useState([]);
  const [author, setAuthor] = useState(" ");
  const [bibliography, setBibliography] = useState(" ");

  const goBackToHomePage = () => {
    props.navigation.navigate("Home");
  };

  const goToPlantWanted = () => {
    const plantWanted = {
      Common_name: plant.common_name,
      Scientific_name: plant.scientific_name,
      ID: ID
    };
    props.navigation.navigate("Quero", {
      plantWanted: plantWanted
    });
  };

  const goToPlantHad = () => {
    const plantHad = {
      Common_name: plant.common_name,
      Scientific_name: plant.scientific_name,
      ID: ID
    };
    props.navigation.navigate("Tenho", {
      plantHad: plantHad
    });
  };

  const load = async () => {
    const response = await Api.get(`/plants/${ID}`);
    console.log(response.data);
    setPlant(response.data);
    setFlower(response.data.main_species.flower);
    setFoliage(response.data.main_species.foliage);
    setFruitOrSeed(response.data.main_species.fruit_or_seed);
    setImage(response.data.images);
    setAuthor(response.data.main_species.author);
    setBibliography(response.data.main_species.bibliography);
  };

  useEffect(() => {
    load();
  }, []);

  return (
    <ScrollView style={Style.container}>
      <View style={Style.headerPage}>
        <TouchableOpacity onPress={goBackToHomePage} style={Style.backArrow}>
          <Image source={require("../../assets/images/BackArrow.png")} />
        </TouchableOpacity>
        <View style={Style.textTitleContainer}>
          <Text style={Style.titleOfPage}>{plant.scientific_name}</Text>
          <Text style={Style.subtitleOfPage}>
            {plant.common_name ? plant.common_name : `não encontrado`}
          </Text>
        </View>
      </View>
      <Image
        source={
          image.url
            ? source(`${image.url}`)
            : require("../../assets/images/cam.jpg")
        }
        style={Style.plantImage}
      />
      <View style={Style.content}>
        <Content
          mainTitleContent={`Nativo de:`}
          subject={plant.native_status ? plant.native_status : `não encontrado`}
        />
        <Content
          mainTitleContent={`Informação completa:`}
          subject={`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur id euismod orci. Vivamus pulvinar, sapien a convallis scelerisque,sapien ante porta lectus, at feugiat orci ligula eu mauris.`}
        />
        <Content
          mainTitleContent={`Duração:`}
          subject={plant.duration ? plant.duration : `não encontrado`}
        />
        <Content
          mainTitleContent={`Cor da Flor:`}
          subject={flower.color ? flower.color : `não encontrado`}
        />
        <List
          title={`Folhagem:`}
          titleSubject={`- Textura:`}
          subject={foliage.texture ? foliage.texture : `não encontrado`}
          titleSubject2={`- Porosidade no Inverno`}
          subject2={
            foliage.porosity_winter ? foliage.porosity_winter : `não encontrado`
          }
          titleSubject3={`- Porosidade no Verão`}
          subject3={
            foliage.porosity_summer ? foliage.porosity_summer : `não encontrado`
          }
          titleSubject4={`- Cor`}
          subject4={foliage.color ? foliage.color : `não encontrado`}
        />
        <List
          title={`Fruta ou Semente:`}
          titleSubject={`- Periodo Inicial:`}
          subject={
            fruitOrSeed.seed_period_begin
              ? fruitOrSeed.seed_period_begin
              : `não encontrado`
          }
          titleSubject2={`- Periodo Final:`}
          subject2={
            fruitOrSeed.seed_period_end
              ? fruitOrSeed.seed_period_end
              : `não encontrado`
          }
          titleSubject3={`- Persistência:`}
          subject3={
            fruitOrSeed.seed_persistence
              ? fruitOrSeed.seed_persistence
              : `não encontrado`
          }
          titleSubject4={`- Abundância:`}
          subject4={
            fruitOrSeed.abundance ? fruitOrSeed.abundance : `não encontrado`
          }
        />
        <Content
          mainTitleContent={`Bibliografia:`}
          subject={bibliography ? bibliography : `não encontrado`}
        />
        <Content
          mainTitleContent={`Autor:`}
          subject={author ? author : `não encontrado`}
        />
      </View>
      <View style={Style.buttonArea}>
        <Button color="#801EB0" title="Tenho" onPress={goToPlantHad} />
        <Button color="#801EB0" title="Quero" onPress={goToPlantWanted} />
      </View>
    </ScrollView>
  );
};

export default showPlant;
