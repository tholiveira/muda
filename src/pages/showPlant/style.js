import React from "react";
import { StyleSheet } from "react-native";
import Colors from "../../themes/colors/colors";

const Style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.screenWhite
  },
  headerPage: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    height: "30%",
    padding: 30,
    alignContent: "center",
    textAlign: "center"
  },
  backArrow: {
    alignSelf: "center",
    marginTop: 20
  },
  textTitleContainer: {
    flexDirection: "column",
    alignSelf: "center",
    marginLeft: 60,
    alignItems: "center"
  },
  titleOfPage: {
    color: Colors.primaryGreen,
    fontSize: 24,
    lineHeight: 48,
    fontFamily: "ubuntu"
  },
  subtitleOfPage: {
    color: Colors.primaryGray,
    fontSize: 14,
    lineHeight: 16,
    fontFamily: "ubuntu"
  },
  plantImage: {
    height: 245,
    width: "100%",
    alignSelf: "center",
    borderWidth: 1,
    borderColor: Colors.primaryGreen
  },
  content: {
    left: 30,
    marginTop: 20
  },
  buttonArea: {
    alignSelf: "center",
    width: "50%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 50
  }
});

export default Style;
