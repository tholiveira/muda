import React, { useState, useEffect } from "react";
import { Text, View } from "react-native";
import Style from "../plantsHave/style";
import { FlatList } from "react-native-gesture-handler";
import Card from "../homepage/components/card/card";

const plantsHave = props => {
  const { navigation } = props;
  const allPlantHad = navigation.getParam("plantHad");
  const [listPlantsHad, setListPlantsHad] = useState([]);

  useEffect(() => {
    load();
  }, [allPlantHad]);

  const load = () => {
    setListPlantsHad([...listPlantsHad, allPlantHad]);
  };

  return (
    <View style={Style.screen}>
      <Text style={Style.title}>Plantas que Tenho</Text>
      <FlatList
        style={Style.list}
        data={listPlantsHad}
        keyExtractor={item => item.ID}
        renderItem={itemData => (
          <Card
            key={itemData.item.ID}
            CommonName={itemData.item.Common_name}
            ScientificName={itemData.item.Scientific_name}
          />
        )}
      />
    </View>
  );
};

export default plantsHave;
