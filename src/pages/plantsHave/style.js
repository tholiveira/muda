import { StyleSheet } from "react-native";
import Colors from "../../themes/colors/colors";

const style = StyleSheet.create({
  screen: {
    backgroundColor: Colors.screenWhite,
    flex: 1,
    paddingBottom: 5
  },
  title: {
    fontSize: 36,
    textAlign: "center",
    color: Colors.primaryBlack,
    marginTop: 30,
    marginBottom: 15,
    fontFamily: "amantic"
  },
  list: {
    flex: 1,
    width: "100%",
    backgroundColor: Colors.screenWhite,
    alignSelf: "center"
  }
});

export default style;
