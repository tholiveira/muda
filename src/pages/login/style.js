import { StyleSheet } from "react-native";
import Colors from "../../themes/colors/colors";

const styles = StyleSheet.create({
  screen: {
    width: "80%",
    alignSelf: "center",
    backgroundColor: Colors.screenWhite
  },
  sectionLogo: {
    alignSelf: "center"
  },
  logo: {
    marginTop: 65,
    marginBottom: 10,
    height: 85,
    width: 85,
    alignSelf: "center"
  },
  title: {
    fontSize: 50,
    textAlign: "center",
    marginBottom: 50,
    textTransform: "uppercase",
    fontFamily: "amantic"
  },
  input: {
    borderColor: Colors.primaryGreen,
    borderWidth: 2,
    borderRadius: 100,
    width: "100%",
    height: 50,
    marginBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
    alignSelf: "center"
  },
  muda: {
    color: Colors.primaryGreen
  },
  sectionButton: {
    justifyContent: "space-between"
  }
});

export default styles;
