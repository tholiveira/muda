import React, { useState } from "react";
import {
  Text,
  View,
  ScrollView,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert
} from "react-native";
import api from "../../services/api";
import Button from "../../components/button/button";
import TextBtn from "../../components/textButton/textButton";
import Style from "./style";

const Login = props => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = async () => {
    try {
      const response = await api.post("auth/sign_in", { email, password });
      setEmail("");
      setPassword("");
      Alert.alert("Logado com sucesso! :)");
      props.navigation.navigate("Home");
    } catch (error) {
      console.log(error);
      Alert.alert(error);
    }
  };

  const handleToSingUp = () => {
    props.navigation.navigate("Register");
  };

  return (
    <ScrollView style={Style.screen}>
      <View style={Style.sectionLogo}>
        <Image
          style={Style.logo}
          source={require("../../assets/images/logo.png")}
        />
        <Text style={Style.title}>
          Vamos <Text style={Style.muda}>Muda</Text>r o Brasil?
        </Text>
      </View>
      <View>
        <View>
          <TextInput
            style={Style.input}
            onChangeText={setEmail}
            value={email}
            placeholder="e-mail"
            placeholderTextColor="#8DB01E"
          />
          <TextInput
            style={Style.input}
            onChangeText={setPassword}
            value={password}
            secureTextEntry={true}
            placeholder="senha"
            placeholderTextColor="#8DB01E"
          />
        </View>
        <View style={Style.sectionButton}>
          <Button onPress={handleLogin} name="Entrar" />
        </View>
        <TextBtn
          sentence1="Não possui conta?"
          sentence2="Cadastre aqui"
          onPress={handleToSingUp}
        />
        <TextBtn sentence1="Esqueeceu sua senha?" sentence2="Resgate aqui" />
      </View>
    </ScrollView>
  );
};

export default Login;
