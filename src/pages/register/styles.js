import { StyleSheet } from "react-native";
import Colors from "../../themes/colors/colors";

const style = StyleSheet.create({
  container: {
    width: "80%",
    alignSelf: "center"
  },
  logo: {
    marginTop: 50,
    marginBottom: 10,
    height: 85,
    width: 85,
    alignSelf: "center"
  },
  title: {
    fontSize: 48,
    fontFamily: "amantic",
    alignSelf: "center",
    marginBottom: 50,
    textTransform: "uppercase"
  },
  input: {
    borderColor: Colors.primaryGreen,
    borderWidth: 2,
    borderRadius: 100,
    width: "100%",
    height: 50,
    marginBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
    alignSelf: "center"
  },
  checkBoxSection: {
    flexDirection: "row",
    textAlignVertical: "center",
    alignItems: "center"
  },
  terms: {
    color: Colors.primaryGreen
  }
});

export default style;
