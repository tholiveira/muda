import React, { useState } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
  CheckBox,
  Image
} from "react-native";

import TextBtn from "../../components/textButton/textButton";
import Button from "../../components/button/button";
import api from "../../services/api";
import Style from "./styles";

const Register = props => {
  const [full_name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password_confirmation, setPasswordConfirmation] = useState("");
  const [checkValue, setCheckValue] = useState(false);

  const handleRegister = async () => {
    try {
      const response = await api.post("auth", {
        full_name,
        email,
        password,
        password_confirmation
      });
      setName("");
      setEmail("");
      setPassword("");
      setPasswordConfirmation("");
      setCheckValue(false);
      handleToLogin();
    } catch (error) {
      Alert.alert("Erro", "dados invalidos", [{ text: "voltar" }]);
      console.log(error);
    }
  };

  const handleToLogin = () => {
    props.navigation.navigate("Login");
  };

  return (
    <ScrollView style={Style.container}>
      <View>
        <Image
          style={Style.logo}
          source={require("../../assets/images/logo.png")}
        />
        <Text style={Style.title}>Cadastro</Text>
      </View>
      <View>
        <TextInput
          style={Style.input}
          onChangeText={setName}
          value={full_name}
          placeholder="nome"
          placeholderTextColor="#8DB01E"
        ></TextInput>
        <TextInput
          style={Style.input}
          onChangeText={setEmail}
          value={email}
          placeholder="e-mail"
          placeholderTextColor="#8DB01E"
        ></TextInput>
        <TextInput
          style={Style.input}
          onChangeText={setPassword}
          value={password}
          placeholder="senha"
          placeholderTextColor="#8DB01E"
          secureTextEntry={true}
        ></TextInput>
        <TextInput
          style={Style.input}
          onChangeText={setPasswordConfirmation}
          value={password_confirmation}
          placeholder="confirmar senha"
          placeholderTextColor="#8DB01E"
          secureTextEntry={true}
        ></TextInput>
      </View>
      <View style={Style.checkBoxSection}>
        <CheckBox onValueChange={setCheckValue} value={checkValue}></CheckBox>
        <Text>Concordo com os</Text>
        <TouchableOpacity>
          <Text style={Style.terms}> termos de uso.</Text>
        </TouchableOpacity>
      </View>
      <Button name="Cadastrar" onPress={handleRegister} />
      <TextBtn
        sentence1="Já possui conta?"
        sentence2="Entre aqui"
        onPress={handleToLogin}
      />
    </ScrollView>
  );
};

export default Register;
