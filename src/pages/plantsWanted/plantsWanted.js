import React, { useState, useEffect } from "react";
import { Text, View } from "react-native";
import Style from "../plantsHave/style";
import { FlatList } from "react-native-gesture-handler";
import Card from "../homepage/components/card/card";

const plantsWanted = props => {
  const { navigation } = props;
  const allPlantWanted = navigation.getParam("plantWanted");
  const [listPlantsWanted, setListPlantsWanted] = useState([]);

  useEffect(() => {
    load();
  }, [allPlantWanted]);

  const load = () => {
    setListPlantsWanted([...listPlantsWanted, allPlantWanted]);
  };

  return (
    <View style={Style.screen}>
      <Text style={Style.title}>Plantas que quero</Text>
      <FlatList
        style={Style.list}
        data={listPlantsWanted}
        keyExtractor={item => item.ID}
        renderItem={itemData => (
          <Card
            key={itemData.item.ID}
            CommonName={itemData.item.Common_name}
            ScientificName={itemData.item.Scientific_name}
          />
        )}
      />
    </View>
  );
};

export default plantsWanted;
