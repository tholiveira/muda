import { StyleSheet } from "react-native";
import Colors from "../../themes/colors/colors";

const Style = StyleSheet.create({
  screen: {
    backgroundColor: Colors.screenWhite,
    flex: 1,
    paddingBottom: 5
  },
  title: {
    fontSize: 36,
    textAlign: "center",
    color: Colors.primaryBlack,
    marginTop: 30,
    marginBottom: 15,
    fontFamily: "amantic"
  },
  helpText: {
    fontSize: 24,
    textAlign: "center",
    color: Colors.primaryBlack
  }
});

export default Style;
