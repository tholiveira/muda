import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  Alert,
  TextInput
} from "react-native";
import { SearchBar } from "react-native-elements";
import Style from "./style";
import Card from "./components/card/card";
import Api from "../../services/plantsApi";
import { array } from "prop-types";

const homePage = props => {
  const [plantsList, setPlantsList] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalPages, setTotalPages] = useState();
  const [searchPlant, setSearchPlant] = useState("");
  const [allPlantsHad, setAllPlantsHad] = useState([]);
  const [allPlantsWanted, setAllPlantsWanted] = useState([]);
  const [filterPlantList, setFilterPlantList] = useState([]);

  const loadMoreOptions = () => {
    if (pageNumber <= totalPages) setPageNumber(pageNumber + 1);
    else {
      Alert.alert("O limite de plantas foi atigindo.");
    }
  };

  const load = async () => {
    const response = await Api.get(`/plants?page=${pageNumber}`);
    setTotalPages(response.headers["total-pages"]);
    setPlantsList([...plantsList, ...response.data]);
  };

  useEffect(() => {
    load();
  }, [pageNumber]);

  const footerComponetn = () => {
    return (
      <TouchableOpacity style={Style.loadMore}>
        <Text style={Style.textLoad}>Carregando mais...</Text>
      </TouchableOpacity>
    );
  };

  const nada = () => {
    return <View />;
  };

  const updateSearch = searchPlant => {
    setSearchPlant(searchPlant);
    setFilterPlantList(
      plantsList.filter(x => {
        return x.slug.includes(searchPlant.toLowerCase());
      })
    );
  };

  return (
    <View style={Style.screen}>
      <View style={Style.searchBar}>
        <SearchBar
          placeholder="pesquisar por plantas..."
          platform="android"
          onChangeText={updateSearch}
          value={searchPlant}
        />
      </View>
      <Text style={Style.title}>Plantas do Mundo</Text>
      <FlatList
        style={Style.list}
        data={filterPlantList.length == 0 ? plantsList : filterPlantList}
        keyExtractor={item => `${item.id}`}
        ListFooterComponent={filterPlantList == 0 ? footerComponetn : nada}
        onEndReached={loadMoreOptions}
        renderItem={itemData => (
          <Card
            key={itemData.item.id}
            CommonName={itemData.item.common_name}
            ScientificName={itemData.item.scientific_name}
            onPress={() => {
              props.navigation.navigate("Show", {
                itemID: itemData.item.id,
                allPlantsHad: allPlantsHad,
                setAllPlantsHad: setAllPlantsHad,
                allPlantsWanted: allPlantsWanted,
                setAllPlantsWanted: setAllPlantsWanted
              });
            }}
          />
        )}
      />
    </View>
  );
};

export default homePage;
