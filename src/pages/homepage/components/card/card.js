import React from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";
import Style from "./style";

const card = props => {
  const goToShowPlants = id => {
    console.log(id);
    // props.navigation.navigate("Show", { params: { id } });
  };

  return (
    <TouchableOpacity onPress={props.onPress} style={Style.card} key={props.id}>
      <View style={Style.containerImage}>
        <Image source={require("../../../../assets/images/leaf.png")} />
      </View>
      <View style={Style.containerTitle}>
        <Text style={Style.titleCard}>{props.ScientificName}</Text>
        {props.CommonName ? (
          <Text style={Style.contentTitle}>{props.CommonName}</Text>
        ) : (
          <Text style={Style.contentTitle}>não encontrado</Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

export default card;
