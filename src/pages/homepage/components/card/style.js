import React from "react";
import { StyleSheet } from "react-native";
import Colors from "../../../../themes/colors/colors";

const Style = StyleSheet.create({
  card: {
    flexDirection: "row",
    borderWidth: 1,
    borderRadius: 20,
    borderColor: Colors.screenWhite,
    backgroundColor: Colors.screenWhite,
    flex: 1,
    height: "10%",
    marginBottom: 10,
    padding: 20,
    elevation: 10,
    width: "90%",
    alignSelf: "center"
  },
  containerTitle: {
    marginLeft: 20,
    flexDirection: "column"
  },
  titleCard: {
    color: Colors.primaryGreen,
    fontSize: 18,
    fontFamily: "ubuntu",
    textTransform: "uppercase"
  },
  contentTitle: {
    color: Colors.primaryGray,
    fontSize: 14,
    fontFamily: "ubuntu"
  },
  containerImage: {
    width: 42,
    height: 42,
    marginTop: 5
  },
  imageCard: {
    flex: 1,
    alignSelf: "stretch"
  }
});

export default Style;
