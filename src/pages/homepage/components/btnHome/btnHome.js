import React from "react";
import { View, TouchableOpacity, Image } from "react-native";
import Style from "./style";

const btnHome = () => {
  return (
    <TouchableOpacity style={Style.btnHome}>
      <Image
        style={Style.cross}
        source={require("../../../../assets/images/btnAdd.png")}
      />
    </TouchableOpacity>
  );
};

export default btnHome;
