import React from "react";
import { StyleSheet } from "react-native";
import Colors from "../../../../themes/colors/colors";

const Style = StyleSheet.create({
  btnHome: {
    flexDirection: "row",
    position: "absolute",
    bottom: 50,
    alignSelf: "flex-end",
    right: 20
  }
});

export default Style;
