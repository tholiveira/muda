import { StyleSheet } from "react-native";
import Colors from "../../themes/colors/colors";

const Style = StyleSheet.create({
  screen: {
    backgroundColor: Colors.screenWhite,
    flex: 1,
    paddingBottom: 5
  },
  searchBar: {
    borderWidth: 1,
    width: "80%",
    borderRadius: 80,
    alignSelf: "flex-end",
    marginTop: 30
  },
  title: {
    fontSize: 36,
    textAlign: "center",
    color: Colors.primaryBlack,
    marginTop: 20,
    marginBottom: 15,
    fontFamily: "amantic"
  },
  list: {
    flex: 1,
    width: "100%",
    backgroundColor: Colors.screenWhite,
    alignSelf: "center"
  },
  loadMore: {
    width: "90%",
    // borderWidth: 1,
    borderColor: Colors.primaryGreen,
    alignSelf: "center",
    borderRadius: 5,
    marginVertical: 20
  },
  textLoad: {
    color: Colors.primaryGreen,
    alignSelf: "center",
    textTransform: "uppercase",
    paddingVertical: 5
  }
});

export default Style;
