import { StyleSheet } from "react-native";
import Colors from "../../themes/colors/colors";

const styles = StyleSheet.create({
  textArea: {
    flexDirection: "row",
    alignSelf: "center",
    marginTop: 20
  },
  linkBtn: {
    color: Colors.primaryGreen,
    marginLeft: 5
  }
});

export default styles;
