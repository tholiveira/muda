import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import Style from "./styles";

const textButton = props => {
  return (
    <View style={Style.textArea}>
      <Text>{props.sentence1}</Text>
      <Text style={Style.linkBtn} onPress={props.onPress}>
        {props.sentence2}
      </Text>
    </View>
  );
};

export default textButton;
