import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";
import Style from "./styles";

const button = props => {
  return (
    <TouchableOpacity onPress={props.onPress} style={Style.button}>
      <Text style={Style.buttonTitle}>{props.name}</Text>
    </TouchableOpacity>
  );
};

export default button;
