import React from "react";
import { StyleSheet } from "react-native";
import Colors from "../../themes/colors/colors";

const styles = StyleSheet.create({
  button: {
    borderRadius: 100,
    backgroundColor: Colors.btnColor,
    width: "100%",
    height: 50,
    padding: 5,
    justifyContent: "center",
    alignSelf: "center",
    marginTop: 15
  },
  buttonTitle: {
    color: Colors.primaryWhite,
    alignSelf: "center",
    textAlignVertical: "center",
    textTransform: "uppercase"
  }
});

export default styles;
