import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import Register from "./pages/register/register";
import Login from "./pages/login/login";
import Navigator from "./services/navegator";
import * as Font from "expo-font";
import { AppLoading } from "expo";

const fetchFonts = () => {
  return Font.loadAsync({
    amantic: require("./assets/fonts/AmaticSC.ttf"),
    amanticBold: require("./assets/fonts/AmaticSC-Bold.ttf"),
    ubuntu: require("./assets/fonts/UbuntuCondensed-Regular.ttf")
  });
};

export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);

  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setFontLoaded(true)}
      />
    );
  }
  return <Navigator />;
}
