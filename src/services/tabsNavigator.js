import React from "react";
import { createBottomTabNavigator } from "react-navigation-tabs";
import homePage from "../pages/homepage/homepage";
import plantsHave from "../pages/plantsHave/plantsHave";
import plantsWanted from "../pages/plantsWanted/plantsWanted";
import help from "../pages/help/help";
import Colors from "../themes/colors/colors";
import { Ionicons } from "@expo/vector-icons";

const tabsNavigator = createBottomTabNavigator(
  {
    Plantas: { screen: homePage },
    Quero: { screen: plantsWanted },
    Tenho: { screen: plantsHave },
    Ajuda: { screen: help }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        console.log("ENTREI NO TABBARICON");
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === "Plantas") {
          iconName = `forward${focused ? "" : "-outline"}`;
        } else if (routeName === "Quero") {
          iconName = `banckward${focused ? "" : "-outline"}`;
        } else if (routeName === "Tenho") {
          iconName = `ios-options${focused ? "" : "-outline"}`;
        } else if (routeName === "Ajuda") {
          iconName = `ios-options${focused ? "" : "-outline"}`;
        }
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: Colors.primaryGreen,
      inactiveTintColor: Colors.primaryGray
    }
  }
);

export default tabsNavigator;
