import axios from "axios";

const api = axios.create({
  baseURL: "https://muda-api.herokuapp.com/"
});

export default {
  post: async (url, body, headers) => {
    return api.post(url, body, headers);
  }
};
