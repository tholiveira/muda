import axios from "axios";

const plantsApi = axios.create({
  baseURL: "https://trefle.io/api/"
});

plantsApi.defaults.headers.common["Authorization"] =
  "Bearer VW9uTnBMVDMwWTlneG43YmFyREVzdz09";

export default {
  get: async (url, headers) => {
    return plantsApi.get(url, headers);
  }
};
