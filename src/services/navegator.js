import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import HomePage from "../pages/homepage/homepage";
import Register from "../pages/register/register";
import Login from "../pages/login/login";
import Show from "../pages/showPlant/showPlant";
import tabsNavigator from "./tabsNavigator";

const MainNavigator = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  },
  Home: {
    screen: tabsNavigator,
    navigationOptions: {
      header: null
    }
  },
  Show: {
    screen: Show,
    navigationOptions: {
      header: null
    }
  },
  Register: {
    screen: Register,
    navigationOptions: {
      header: null
    }
  }
});

const Nave = createAppContainer(MainNavigator);

export default Nave;
