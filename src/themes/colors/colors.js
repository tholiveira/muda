const colors = {
  btnColor: "#801EB0",
  primaryWhite: "#fafafa",
  primaryBlack: "#333333",
  primaryGreen: "#8DB01E",
  primaryGray: "#C4C4C4",
  screenWhite: "#FFFFFF"
};

export default colors;
